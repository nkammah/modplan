// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.atlassian.bamboo.repository.RepositoryDefinitionManager;

import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.spring.container.ContainerManager;

import java.lang.String;
import java.util.*;
import java.util.regex.Pattern;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.event.EventManager;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.build.creation.RepositoryConfigHelper;
import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.CronTriggerBuildStrategy;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.RepositoryDefinition;
import com.atlassian.bamboo.repository.RepositoryDefinitionEntity;
import com.atlassian.bamboo.caching.DashboardCachingManager;
import com.atlassian.bamboo.event.BuildConfigurationUpdatedEvent;
import com.atlassian.bamboo.task.TaskDefinition;
import org.apache.log4j.Logger;
import org.apache.commons.configuration.HierarchicalConfiguration;

import com.atlassian.bamboo.variable.VariableDefinition;

@Path("/{plankey}")
public class ModplanResource {
    
    private static final Logger log = Logger.getLogger(ModplanResource.class);
    private EventManager eventManager;
    private PlanManager planManager;
    private DashboardCachingManager dashboardCachingManager;
    private VariableDefinitionManager varMgr;
	private ProjectManager projectManager;

    /**
     * This method will be called if no extra path information
     * is used in the request.
     * @param key optional key for the message
     * @return the message from the key parameter or the default message
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getBuild(@PathParam("plankey") String plankey)
    {

        log.debug("ModplanResource.getBuild()");

        if (plankey == null) {
            log.info("plankey was null");
            return Response.ok(new Message("default","plankey was null")).build();
        }

        String mesg = "Return configuration for plan:  " + plankey;
        log.info(mesg);

        return Response.ok(new Message("default", mesg)).build();
    }


    // wget http://localhost:8085/rest/modplan/1.0/PROJ1-PLAN1/repository
    // Return a response containing all the configuration values for all the
    // Plan-level repositories
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/repository")
    public Response getRepositoryConfig(@PathParam("plankey") String plankey)
    {
        log.info("ModplanResource.getRepositoryConfig()");

        try {
            Plan plan = getPlanByKey(plankey);
            return listRepositoryConfiguration(plankey);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getRepositoryConfig()");
            return re.getResponse();
        }
    }


    // wget http://localhost:8085/rest/modplan/1.0/PROJ1-PLAN1/
    //     repository/1/repository.common.cleanCheckout
    // Get the value of a single repository configuration field
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/repository/{position}/{field}")
    public Response getRepositoryValue(
                    @PathParam("plankey") String plankey, 
                    @PathParam("position")String position,
                    @PathParam("field") String field) throws ResponseException
    {
        log.info("ModplanResource.getRepositoryValue()");
        int pos;
        try {
        	pos = Integer.parseInt(position);
        }
        catch(NumberFormatException nfe) {
           log.error("Invalid integer value in path: " + position);
           throw new ResponseException(Response.Status.BAD_REQUEST);
        }
        try {
            Plan plan = getPlanByKey(plankey);
            return getRepositoryConfigValue(plan, pos, field);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getRepositoryConfig()");
            return re.getResponse();
        }
    }


    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/repository/
    //      repository.common.cleanCheckout
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    //
    //  Set the value of a single repository configuration field    
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/repository/{position}/{field}")
    public Response setRepositoryValue( @PathParam("plankey") String plankey,
    									@PathParam("position")String position,
                                        @PathParam("field")   String field,
                                                              String value) throws ResponseException
    {
        log.info ("ModplanResource.setRepositoryValue(): " + value);
        int pos;
        try {
        	pos = Integer.parseInt(position);
        }
        catch(NumberFormatException nfe) {
           log.error("Invalid integer value in path: " + position);
           throw new ResponseException(Response.Status.BAD_REQUEST);
        }
        
        try {
            Plan plan = getPlanByKey(plankey);
            return setRepositoryConfigValue(plan, pos, field, value);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setRepositoryValue()");
            return re.getResponse();
        }
    }


    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/variables")
    public Response getPlanVariables(@PathParam("plankey") String plankey)
    {
        log.info("ModplanResource.getPlanVariables()");

        try {
            Plan plan = getPlanByKey(plankey);
            return listPlanVariables(plankey);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getPlanVariables()");
            return re.getResponse();
        }
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/variables/{varname}")
    public Response getPlanVariable(
                    @PathParam("plankey") String plankey,
                    @PathParam("varname") String varname)
    {
        log.info("ModplanResource.getPlanVariable()");

        try {
            Plan plan = getPlanByKey(plankey);
            return getPlanVariableValue(plan, varname);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getPlanVariableValue()");
            return re.getResponse();
        }
    }

    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/variables/foo.bar
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    //
    //  Set the value of a single plan variable
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/variables/{varname}")
    public Response setPlanVariable( @PathParam("plankey") String plankey,
                                     @PathParam("varname") String varname,
                                                           String value)
    {
        log.info ("ModplanResource.setPlanVariable(): " + value);
        try {
            Plan plan = getPlanByKey(plankey);
            return setPlanVariableValue(plan, varname, value);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setPlanVariableValue()");
            return re.getResponse();
        }
    }

    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/project/{key}")
    public Response createProject( @PathParam("plankey") String plankey,
                                   @PathParam("key") String key,
                                                     String name)
    {
        log.info ("ModplanResource.createProject(): " + key);
        if (projectManager.getProjectByKey(key) == null){
			Project project = projectManager.createProject(key, name);
			projectManager.saveProject(project);
		    return Response.ok(new Message("enabled", "Project created")).build();

		}
		return Response.ok(new Message("enabled", "Project "+key+" exists already")).build();
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/enabled")
    public Response getEnabled( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getEnabled()");

        try {
            Plan plan = getPlanByKey(plankey);
            log.info("Returning plan enabled status");

            String b;
            if (plan.isSuspendedFromBuilding())
                b = "false";
            else
                b = "true";

            return Response.ok(new Message("enabled", b)).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }


    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/enabled
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/enabled")
    public Response setEnabled( @PathParam("plankey") String plankey,
                                                      String value)
    {
        log.info ("ModplanResource.setEnabled(): " + value);

        value = value.trim();
        try {
            Plan plan = getPlanByKey(plankey);

            String mesg = "";
            if(value.equals("suspend") || value.equals("disable")) {
                planManager.setPlanSuspendedState(plan, true);
                mesg = "Suspended plan: " + plankey;
                log.info(mesg);
                //updateDashboardCache(plankey);   
            }
            else if(value.equals("resume") || value.equals("enable")) {
                planManager.setPlanSuspendedState(plan, false);
                mesg = "Resumed build: " + plankey;
                log.info(mesg);
                //updateDashboardCache(plankey);   
            }
            else {
                mesg = "Invalid value: " + value;
                log.error(mesg);
                throw new ResponseException(Response.Status.BAD_REQUEST);
            }
            return Response.ok(new Message("enabled", mesg)).build();
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setEnabled()");
            return re.getResponse();
        }
    }

    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/enabled/0
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/enabled/{jobindex}")
    public Response setJobEnabled( @PathParam("plankey")  		String plankey,
    								@PathParam("jobindex") 		String jobIndex,
    															String value)
    {
        log.info ("ModplanResource.setJobEnabled()");
        
        value = value.trim();

        try {

            checkManagersEx();
            Plan plan = getPlanByKey(plankey);
            Job job = getJobFromPlan(plankey, jobIndex);
            
            String mesg = "";
            if(value.equals("suspend") || value.equals("disable")) {
            	job.setSuspendedFromBuilding(true);
                mesg = "Suspended job " + plankey + " " + job.getKey() ;
                log.info(mesg);
            }
            else if(value.equals("resume") || value.equals("enable")) {
            	job.setSuspendedFromBuilding(false);
                planManager.setPlanSuspendedState(plan, false);
                mesg = "Resumed job " + plankey + " " + job.getKey() ;
                log.info(mesg);
            }
            else {
                mesg = "Invalid value: " + value;
                log.error(mesg);
                throw new ResponseException(Response.Status.BAD_REQUEST);
            }
            planManager.savePlan(plan);

            return Response.ok(new Message("enabled", mesg)).build();

        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setJobEnabled()");
            return re.getResponse();
        }


    }  
    

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/types")
    public Response getTypes( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getTypes()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }

        BuildDefinition definition = plan.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        String instanceList = instanceList(plan);
        return Response.ok(new Message("Instance list", instanceList)).build();

    }        

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/strategy")
    public Response getBuildStrategy( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getBuildStrategy()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }

        BuildDefinition definition = plan.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        BuildStrategy strategy = definition.getBuildStrategy(); 

        KVList list = new KVList();
        list.addPair(new KVPair("name", strategy.getName()));
        
        HierarchicalConfiguration config = strategy.toConfiguration();
        Iterator<String> iter = config.getKeys();


        
        while (iter.hasNext()) {
            String key = iter.next();
            Object property = config.getProperty(key);
            KVPair pair = new KVPair(key, property.toString());
            list.addPair(pair);
        }

        return   Response.ok(list).build();
    } 

    
    /*
     * curl -d <cron_expression> -H "Content-Type:text/plain"
         http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/strategy/schedule?os_authType=basic&os_username=dolby&os_password=dolby
     * 
     * Format for cron strategy : seconds, minutes, hours, day-of-month, month, day-of-week and (optional) year
     * 
     * day-of-week : 1-sunday, 2-monday, 3-tuesday...7-saturday
     * 
     */
    
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/strategy/cron/")
    public Response setScheduleStrategy( @PathParam("plankey") String plankey,
    								  							String value)
    {
        log.debug("ModplanResource.getBuildStrategy()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }

        BuildDefinition definition = plan.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        CronTriggerBuildStrategy strategy = new CronTriggerBuildStrategy();
        strategy.setCronExpression(value);
        definition.setBuildStrategy(strategy);
        ContainerManager.autowireComponent(strategy);
        return Response.ok(new Message("ScheduleStrategy", value)).build();

    } 
    
    
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs")
    public Response getJobs( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getJobs()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }

        return listJobsForPlan(plan);
    }       

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/stages")
    public Response getStages( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getStages()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
        return listStagesForPlan(plan);
    }       

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs/{jobindex}/tasks")
    public Response getTasksForJob( @PathParam("plankey") String plankey,
                                  @PathParam("jobindex") String index)
    {
        log.debug("ModplanResource.getTasksForJob()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, index);
            return listTasksForPlan(job);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }       

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs/{jobindex}/tasks/{taskindex}")
    public Response getTaskConfig( @PathParam("plankey")   String plankey,
                                   @PathParam("jobindex")  String jobIndex,
                                   @PathParam("taskindex") String taskIndex)
    {
        log.debug("ModplanResource.getTaskConfig()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, taskIndex);
            TaskDefinition td = getTaskDefinitionAtIndex(job, taskIndex);

            Map<String,String> map = td.getConfiguration();
            KVList k = mapToKVList(map);
            return Response.ok(k).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }

    //  http://localhost:8085/rest/modplan/1.0/PROJ1-PLAN1/jobs/0/tasks/0/argument
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs/{jobindex}/tasks/{taskindex}/{field}")
    public Response getTaskConfigField( @PathParam("plankey") String plankey,
                                   @PathParam("jobindex")     String jobIndex,
                                   @PathParam("taskindex")    String taskIndex,
                                   @PathParam("field")        String field)
    {
        log.debug("ModplanResource.getTaskConfigField()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, taskIndex);
            TaskDefinition td = getTaskDefinitionAtIndex(job, taskIndex);

            Map<String,String> map = td.getConfiguration();
            if(! map.containsKey(field)) {
                log.error("No such field in task at index: " + taskIndex);
                throw new ResponseException(Response.Status.BAD_REQUEST);
            }
            return Response.ok(new Message(field, map.get(field))).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }

    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/jobs/0/tasks/0/argument
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/jobs/{jobindex}/tasks/{taskindex}/{field}")
    public Response setTaskConfigField( @PathParam("plankey") String plankey,
                                   @PathParam("jobindex")     String jobIndex,
                                   @PathParam("taskindex")    String taskIndex,
                                   @PathParam("field")        String field,
                                                              String value)
    {
        log.debug("ModplanResource.setTaskConfigField()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, taskIndex);
            TaskDefinition td = getTaskDefinitionAtIndex(job, taskIndex);

            Map<String,String> map = td.getConfiguration();
            if(!map.containsKey(field)) {
                log.error("No such field in task at index: " + taskIndex);
                throw new ResponseException(Response.Status.BAD_REQUEST);
            }
            map.put(field, value);
            td.setConfiguration(map);
            return Response.ok(new Message(field, value)).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }

    
    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/name
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/name")
    public Response setPlanName( @PathParam("plankey") String plankey,
                                                       String planname)
    {
        log.info ("ModplanResource.setBuildName(): " + planname);
        try {
            Plan plan = getPlanByKey(plankey);
            plan.setBuildName(planname);
            planManager.savePlan(plan);
            String mesg = "Renamed Plan to  " + planname ;
            return Response.ok(new Message("default", mesg)).build();
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setPlanVariableValue()");
            return re.getResponse();
        }
    }

    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/description
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/description")
    public Response setPlanDescription( @PathParam("plankey") String plankey,
                                                       		  String plandescription)
    {
        log.info ("ModplanResource.setBuildName(): " + plandescription);
        try {
            Plan plan = getPlanByKey(plankey);
            plan.setDescription(plandescription);
            planManager.savePlan(plan);
            String mesg = "Updated plan description to  " + plandescription ;
            return Response.ok(new Message("default", mesg)).build();
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setPlanVariableValue()");
            return re.getResponse();
        }
    }

    
    
    //curl http://<host>:<port>/rest/modplan/1.0/requirement/PROJ1-PLAN1/0
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/requirement/{jobindex}")
    public Response listJobRequirement( @PathParam("plankey")  		String plankey,
    									   @PathParam("jobindex") 		String jobIndex)
    {
        log.info ("listJobRequirement()");
        
        KVList list = new KVList();

        try {

            checkManagersEx();
            Plan plan = getPlanByKey(plankey);
            Job job = getJobFromPlan(plankey, jobIndex);
            // Get the current requirement set
            RequirementSet requirementSet = job.getRequirementSet();

            for (Requirement req:requirementSet.getRequirements()){
                KVPair pair = new KVPair(req.getKey(), req.getMatchValue().toString());
                list.addPair(pair);
            }
            
            return Response.ok(list).build();

        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.listJobRequirement()");
            return re.getResponse();
        }
    }  
    
    //curl http://<host>:<port>/rest/modplan/1.0/requirement/PROJ1-PLAN1/0/requKey:false:myValue
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/requirement/{jobindex}/{requirementKey}")
    public Response getJobRequirement( @PathParam("plankey")  		String plankey,
    									   @PathParam("jobindex") 		String jobIndex,
    									   @PathParam("requirementKey") String requirementKey)
    {
        log.info ("ModplanResource.getJobRequirement()");
        
        KVList list = new KVList();

        try {

            checkManagersEx();
            Plan plan = getPlanByKey(plankey);
            Job job = getJobFromPlan(plankey, jobIndex);
            // Get the current requirement set
            RequirementSet requirementSet = job.getRequirementSet();

            Pattern pattern = Pattern.compile(requirementKey);
            for (Requirement req:requirementSet.getRequirements()){
            	if (pattern.matcher(req.getKey()).matches()){
                        KVPair pair = new KVPair(req.getKey(), req.getMatchValue().toString());
                        list.addPair(pair);
                    } 
            }
            
            return Response.ok(list).build();

        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getJobRequirement()");
            return re.getResponse();
        }
    }  
    
    //curl -X PUT --user username:password 
    //	http://<host>:<port>/rest/modplan/1.0/requirement/PROJ1-PLAN1/0/requKey:false:myValue
    //	?os_authType=basic
    @PUT
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/requirement/{jobindex}/{requirementKey}:{regexMatch}:{match}")
    public Response updateJobRequirement( @PathParam("plankey")  		String plankey,
    									   @PathParam("jobindex") 		String jobIndex,
    									   @PathParam("requirementKey") String requirementKey,
    									   @PathParam("regexMatch") 	String regexMatch,
    									   @PathParam("match") 			String match)
    {
        log.info ("ModplanResource.setPlanJobRequirement()");
        
    	boolean isRegexMatch = Boolean.parseBoolean(regexMatch);

        try {
            checkManagersEx();
            Plan plan = getPlanByKey(plankey);
            Job job = getJobFromPlan(plankey, jobIndex);
            // Get the current requirement set
            RequirementSet requirementSet = job.getRequirementSet();
            
            // Remove the requirement
            requirementSet.removeRequirement(requirementKey);

            // Add the udpated requirement
            requirementSet.addRequirement(new RequirementImpl(requirementKey, isRegexMatch, match));
            // Save changes
            planManager.savePlan(plan);
            
            String mesg = "Requirement added";
            return Response.ok(new Message("default", mesg)).build();

        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.updateJobRequirement()");
            return re.getResponse();
        }
    }    
    

    private TaskDefinition getTaskDefinitionAtIndex(Job job, String taskIndex) throws ResponseException
    {
         int index;
         try {
             index = Integer.parseInt(taskIndex);
        }
        catch(NumberFormatException nfe) {
            log.error("Invalid integer value in path: " + taskIndex);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        BuildDefinition definition = job.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        java.util.List<TaskDefinition> tdlist = definition.getTaskDefinitions();

        try {
            TaskDefinition td = tdlist.get(index);
            return td;
        }
        catch(IndexOutOfBoundsException e) {
             log.error("No job at index: " + index);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }

    }
    
    private Job getJobFromPlan(String plankey, String index) throws ResponseException
    {
        Plan plan = planManager.getPlanByKey(plankey);
        if(plan == null) {
            log.error("No such plan:" + plankey);
            throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        if(!(plan instanceof com.atlassian.bamboo.chains.Chain)) {
            log.error("Plan is not an instance of Chain");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        Chain chain = (Chain) plan;
        java.util.List<Job> jobList = chain.getAllJobs();
        Job job;
        int indexNum;

        try {
            indexNum = Integer.parseInt(index);
        }
        catch(NumberFormatException nfe) {
            log.error("Invalid integer value in path: " + index);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        try {
            job = jobList.get(indexNum);
        }
        catch(IndexOutOfBoundsException ioobe) {
            log.error("No job at index: " + index);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }
        return job;
    }

    private Response listJobsForPlan(Plan plan) {

        if(!(plan instanceof com.atlassian.bamboo.chains.Chain)) {
            log.error("Plan is not an instance of Chain");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        Chain chain = (Chain) plan;
        java.util.List<Job> jobList = chain.getAllJobs();


        Iterator<Job> iter = jobList.iterator();
        StringList sl = new StringList();

        while (iter.hasNext()) {
            Job job = iter.next();
            sl.add(job.getName());
        } 

        return Response.ok(sl).build();
    }

    private Plan getPlanByKey(String planKey) throws ResponseException {

        checkManagersEx();

        Plan plan = planManager.getPlanByKey(planKey);
        if(plan == null) {
            log.error("No such plan:" + planKey);
            throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        return plan;
    }


    private Response listStagesForPlan(Plan plan) {

        if(!(plan instanceof com.atlassian.bamboo.chains.Chain)) {
            log.error("Plan is not an instance of Chain");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        Chain chain = (Chain) plan;
        java.util.List<ChainStage> stageList = chain.getStages();


        Iterator<ChainStage> iter = stageList.iterator();
        StringList sl = new StringList();

        while (iter.hasNext()) {
            ChainStage stage = iter.next();
            sl.add(stage.getName());
        } 

        return Response.ok(sl).build();
    }


    private String instanceList(Plan plan) {
            
        String result = new String();

        if(plan instanceof com.atlassian.bamboo.build.Build) {
            result += "Build ";
        }
        if(plan instanceof com.atlassian.bamboo.build.Buildable) {
            result += "Buildable ";
        }
        if(plan instanceof com.atlassian.bamboo.chains.Chain) {
            result += "Chain ";
        }
        if(plan instanceof com.atlassian.bamboo.build.Job) {
            result += "Job ";
        }
        if(plan instanceof com.atlassian.bamboo.plan.TopLevelPlan) {
            result += "TopLevelPlan ";
        }

        if (plan instanceof com.atlassian.bamboo.plan.PlanPermissionSkeleton) {
            result += "PlanPermissionSkeleton ";        
        }
        
        if (result.length() == 0) {
            result = "Not an instance of any known type";
        }
        return result; 
    }


    private Response listTasksForPlan(Plan plan) {

        BuildDefinition definition = plan.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        java.util.List<TaskDefinition> tdlist = definition.getTaskDefinitions();
        log.info("Number of task definitions is: " + Integer.toString(tdlist.size()));

        Iterator<TaskDefinition> iter = tdlist.iterator();
        
        StringList sl = new StringList();

        while (iter.hasNext()) {
            TaskDefinition td = iter.next();
            sl.add(td.getUserDescription());
        } 

        return Response.ok(sl).build();
    }


    private KVList mapToKVList(Map<String, String> map) {

        KVList list = new KVList();
        Set<String> keys = map.keySet();
        Iterator<String> iter = keys.iterator();

        while(iter.hasNext()) {
            String key = iter.next();
            String value = map.get(key);
            KVPair pair = new KVPair(key, value);
            list.addPair(pair);
        }

        return list;
    }


    private Response listRepositoryConfiguration(String plankey) {
        Plan plan = planManager.getPlanByKey(plankey);
    	RepositoryDefinitionManager repositoryDefinitionManager = (RepositoryDefinitionManager) ContainerManager.getComponent("repositoryDefinitionManager");
        
    	Tree repoList = new Tree();
    	
    	for (RepositoryDefinition repositoryDefinition : repositoryDefinitionManager.getRepositoryDefinitionsForPlan(plan))
    	{
        	KVList list = new KVList();
	    	Repository repository = repositoryDefinition.getRepository();
    		HierarchicalConfiguration config = repository.toConfiguration();
    		list.addPair(new KVPair("name", repositoryDefinition.getName()));
    		list.addPair(new KVPair("position", Integer.toString(repositoryDefinition.getPosition())));
    		list.addPair(new KVPair("description", repositoryDefinition.getDescription()));

    		Iterator<String> iter = config.getKeys();
            while (iter.hasNext()) {
                String key = iter.next();
                Object property = config.getProperty(key);
                KVPair pair = new KVPair(key, property.toString());
                list.addPair(pair);
            }
            repoList.addNode(list);
    	}
        return Response.ok(repoList).build();
    }

    private Response setRepositoryConfigValue(Plan plan, int position, String key, String value)
    {
    	RepositoryDefinitionManager repositoryDefinitionManager = (RepositoryDefinitionManager) ContainerManager.getComponent("repositoryDefinitionManager");
    	for (RepositoryDefinition repositoryDefinition : repositoryDefinitionManager.getRepositoryDefinitionsForPlan(plan))
    	{
    	    if (repositoryDefinition.getPosition() == position){
    	    	Repository repository = repositoryDefinition.getRepository();
    	    
        		HierarchicalConfiguration config = repository.toConfiguration();
        		config.setProperty(key, value);
        		repository.populateFromConfig(config);
    	        RepositoryDefinitionEntity rde = repositoryDefinitionManager.getRepositoryDefinitionEntity(repositoryDefinition.getId());
    	        if (rde != null)
    	        {
    	            rde.setXmlData(RepositoryConfigHelper.prepareXmlConfiguration(repository, repositoryDefinition.getWebRepositoryViewer()));
    	        }
    	        repositoryDefinitionManager.saveRepositoryDefinition(rde);
    	    
    	        planManager.savePlan(plan);
    	        eventManager.publishEvent(new BuildConfigurationUpdatedEvent(this, plan.getPlanKey()));
    	        String mesg = "Set key: " + key + " to value: " + value;
    	        return Response.ok(new Message("default", mesg)).build();
    	    }
    	}
        String mesg = "Repository not found";
        return Response.ok(new Message("default", mesg)).build();
    }


    private Response getRepositoryConfigValue(Plan plan, int position, String key)
    {
    	RepositoryDefinitionManager repositoryDefinitionManager = (RepositoryDefinitionManager) ContainerManager.getComponent("repositoryDefinitionManager");
    	for (RepositoryDefinition repositoryDefinition : repositoryDefinitionManager.getRepositoryDefinitionsForPlan(plan))
    	{
    	    if (repositoryDefinition.getPosition() == position){
    	    	if (key == "name"){
        	        return Response.ok(new Message(key, repositoryDefinition.getName())).build();
    	    	}else if (key=="description"){
        	        return Response.ok(new Message(key, repositoryDefinition.getDescription())).build();
    	    	}else{
        	    	Repository repository = repositoryDefinition.getRepository();
        	        HierarchicalConfiguration config = repository.toConfiguration();
        	        String value = config.getProperty(key).toString();
        	        return Response.ok(new Message(key, value)).build();  		
    	    	}

    	    }
    	}
        String mesg = "Repository not found";
        return Response.ok(new Message("default", mesg)).build();
    }


     private Response listPlanVariables(String plankey)
     {
         Plan plan = planManager.getPlanByKey(plankey);
         java.util.List<VariableDefinition> varList = varMgr.getPlanVariables(plan);
         Iterator<VariableDefinition> iter = varList.iterator();

         KVList list = new KVList();
         while (iter.hasNext()) {
             VariableDefinition var = iter.next();
             KVPair pair = new KVPair(var.getKey(), var.getValue());
             list.addPair(pair);
         }

         return Response.ok(list).build();
    }

    private Response getPlanVariableValue(Plan plan, String varname)  throws ResponseException
    {
        java.util.List<VariableDefinition> varList = varMgr.getPlanVariables(plan);
        Iterator<VariableDefinition> iter = varList.iterator();

        while (iter.hasNext()) {
             VariableDefinition var = iter.next();
             if (var.getKey().equals(varname)) {
                 return Response.ok(new Message(varname, var.getValue())).build();
             }
        }
         log.error("No variable '" + varname + "' defined for plan: "+ plan.getKey());
         throw new ResponseException(Response.Status.BAD_REQUEST);
    }


    private Response setPlanVariableValue(Plan plan, String varname, String value) throws ResponseException
    {
        java.util.List<VariableDefinition> varList = varMgr.getPlanVariables(plan);
        Iterator<VariableDefinition> iter = varList.iterator();

        while (iter.hasNext()) {
             VariableDefinition var = iter.next();
             if (var.getKey().equals(varname)) {
                 var.setValue(value);
                 varMgr.saveVariableDefinition(var);
                 String mesg = "Set variable: " + varname + " to value: " + value;
                return Response.ok(new Message("default", mesg)).build();
             }
        }

        // If we get here the variable doesn't yet exist
        String mesg = "Plan variable: " + varname + " does not exist";
        log.error(mesg);
        throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
    }


    private void checkManagersEx() throws ResponseException {

        if(planManager == null) {
            log.error("planManager is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        if(eventManager == null) {
            log.error("eventManager is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        if(dashboardCachingManager == null) {
            log.error("dashboardCachingManager is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public void setEventManager(EventManager eventManager)
    {
        this.eventManager = eventManager;
    }
   
    public void setPlanManager(PlanManager planManager)
    {
        this.planManager = planManager;
    }

    public void setProjectManager(ProjectManager projectManager){
        this.projectManager = projectManager;
    }
    
    public void setDashboardCachingManager(DashboardCachingManager mgr)
    {
        this.dashboardCachingManager = mgr;
    }

    public void setVariableDefinitionManager(VariableDefinitionManager mgr)
    {
        this.varMgr = mgr;
    }
    
}


