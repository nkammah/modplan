// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import javax.xml.bind.annotation.*;

/**
 * A message returned from the {@link ModplanResource}
 */
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {
    @XmlAttribute
    private String key;

    @XmlElement(name = "value")
    private String message;

    public Message() {
    }

    public Message(String key, String message) {
        this.key = key;
        this.message = message;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
