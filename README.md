The modplan plugin exposes a REST API that allows querying and modifying build plan configurations.

**Note**:
Most invalid request will return exception status code without providing details on the exception - details are logged in the bamboo log only ( at this time).

##Project actions
###Create a new project
Creating projects is usually handy if you want to use the Bamboo Clone API for a new project not yet defined.  
This endpoint is really a hack and should not be part of this plugin - you can use any dummy string for the PLAN-KEY as it is not used.

`curl --user admin:admin -d "Project Name" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/DUMMY/project/test`

##Plan actions
###Rename a plan
`curl --user admin:admin -d "my new name" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/PLAN-KEY/name`

###Update description
`curl --user admin:admin -d "my new description" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/PLAN-KEY/description`

###Check if plan is enabled
`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/enabled`

###Enable/Disable plan
**disable values** : suspend, disable  
**enable values** : resume, enable  

`curl --user admin:admin -d "suspend" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/PLAN-KEY/enabled`

##Jobs actions
###List jobs
`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/jobs`

###Enable/Disable job
Enable/disable a JOB based on the JOB index  
**disable values** : suspend, disable  
**enable values** : resume, enable  

`curl --user admin:admin -d "suspend" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/PLAN-KEY/enabled/0`

##Stages actions
###List stages
`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/stages`

##Plan Variables actions
###List plan variables
`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/variables`

###Get a specific plan variable's value
`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/variables/var1`

###Update a specific plan variable's value
`curl --user admin:admin -d "new_value" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/PLAN-KEY/variables/var1`


##Repository actions
### List all the repositories and their properties for a given plan
`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/repository/`

###Get a specific property for a given plan and repository ( identified by the repository position)
`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/repository/1/repository.git.repositoryUrl`

###Update a specific property for a given plan and repository ( identified by the repository position)
`curl --user admin:admin -d "http://bitbucket.org/nkammah/modplan.git" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/PLAN-KEY/repository/0/repository.git.repositoryUrl`


##Build Strategy actions
###Get Strategy
This call will return the build strategy ( with the cron expression for a cron strategy)

`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/strategy`

###Set a cron build strategy
`curl --user admin:admin -d "0 0 */3 ? * *" -H "Content-Type:text/plain" http://localhost:8085/rest/modplan/1.0/PLAN-KEY/strategy/cron``


##Requirements actions
###Get a requirement
Get a requirement value for a given plan and a given job ( identified by it's index)

`curl http://localhost:8085/rest/modplan/1.0/PLAN-KEY/requirement/0/REQUIREMENT_NAME`

###Set a requirement
Set a requirement value for a given plan,  a given job and a requirement name( identified by it's index)

`curl -X PUT --user admin:admin http://localhost:8085/rest/modplan/1.0/PLAN-KEY/requirement/0/REQUIREMENT_NAME:REGEX_MATCH:VALUE`  
**REQUIREMENT_NAME** : the name of the requirement  
**REGEX_MATCH** :  true --> "matches" the requirement, false --> "equals" the requirements  
**VALUE** : the value for the requirement. Use ".*" to declare that a requirement must exist